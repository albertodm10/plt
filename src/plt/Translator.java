package plt;

public class Translator {
	
	public static final String NIL ="nil";

	private String inputPhrase;
	private String[] words;
	private String outputPhrase;
	private String regexWithHyphen = "[A-Za-z.,;:?!'()\s-]*";
	
	public Translator(String phrase) {
		inputPhrase = phrase;
		words = inputPhrase.split(" ");
	}

	public String getInputPhrase() {
		return inputPhrase;
	}

	public String translate() throws PigLatinException {
		if(!isEmpty()) {
			if(!inputPhrase.matches(regexWithHyphen)) {
				throw new PigLatinException();
			}
			else {
				for(int i=0; i<words.length; i++) {
					String outputWord = "";
					
					if(words[i].contains("-")) {
						outputWord = translateWordWithHyphen(words[i]);
					}
					else if(words[i].contains(".")) {
						outputWord = translateWordWithSpecialCharacter(".", words[i]);
					}
					else if(words[i].contains(",")) {
						outputWord = translateWordWithSpecialCharacter(",", words[i]);
					}
					else if(words[i].contains(";")) {
						outputWord = translateWordWithSpecialCharacter(";", words[i]);
					}
					else if(words[i].contains(":")) {
						outputWord = translateWordWithSpecialCharacter(":", words[i]);
					}
					else if(words[i].contains("?")) {
						outputWord = translateWordWithSpecialCharacter("?", words[i]);
					}
					else if(words[i].contains("!")) {
						outputWord = translateWordWithSpecialCharacter("!", words[i]);
					}
					else if(words[i].contains("'")) {
						outputWord = translateWordWithSpecialCharacter("'", words[i]);
					}
					else if(words[i].contains("(")) {
						outputWord = translateWordWithSpecialCharacter("(", words[i]);
					}
					else if(words[i].contains(")")) {
						outputWord = translateWordWithSpecialCharacter(")", words[i]);
					}
					else {
						outputWord = translateWord(words[i]);
					}
					
					if (i==0) {
						outputPhrase = outputWord;
					}
					else {
						outputPhrase = outputPhrase + " " + outputWord;
					}
				}
				return outputPhrase;
			}
		}
		return NIL;
	}
	
	public boolean isEmpty() {
		return inputPhrase.equals("");
	}
	
	public boolean startWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u");
	}
	
	public boolean endWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u");
	}
	
	public boolean isVowel(char letter) {
		return letter=='a' || letter=='e' || letter=='i' || letter=='o' || letter=='u'; 
	}
	
	public boolean isCurrentLetterVowel(int index, String word) {
		return isVowel(word.charAt(index));
	}
	
	public String translateWord (String word) {
		String outputWord = "";
		
		if(startWithVowel(word)) {
			if(word.endsWith("y")) {
				outputWord = word + "nay";
			}
			else {
				if(endWithVowel(word)) {
					outputWord = word + "yay";
				}
				else {
					outputWord = word + "ay";
				}
			}
		}
		else {
			int i=0;
			outputWord = word;
			
			while (!isCurrentLetterVowel(i, word)) {
				outputWord = outputWord.substring(1) + outputWord.charAt(0);
				i++;
			}
			outputWord = outputWord + "ay";
		}
		return outputWord;
	}
	
	public String translateWordWithSpecialCharacter (String specialCharacter, String word) {
		String outputWord = "";
		String[] compositeWords = word.split("\\" + specialCharacter);
		
		for(int i=0; i<compositeWords.length; i++) {
			String outputCompositeWord = translateWord(compositeWords[i]);
			outputWord = outputWord + outputCompositeWord + specialCharacter;	
		}
		return outputWord;
	}
	
	public String translateWordWithHyphen (String word) {
		String outputWord = "";
		String[] compositeWords = word.split("\\-");
		
		for(int i=0; i<compositeWords.length; i++) {
			String outputCompositeWord = translateWord(compositeWords[i]);
			
			if (i < (compositeWords.length-1)) {
				outputWord = outputWord + outputCompositeWord + "-";
			}
			else if (i == (compositeWords.length-1)) {
				outputWord = outputWord + outputCompositeWord;
			}
		}
		return outputWord;
	}

}
