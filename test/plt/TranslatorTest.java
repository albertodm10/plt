package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getInputPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() throws PigLatinException {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() throws PigLatinException {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() throws PigLatinException {
		String inputPhrase = "unity";
		Translator translator = new Translator(inputPhrase);
		assertEquals("unitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() throws PigLatinException {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() throws PigLatinException {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() throws PigLatinException {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() throws PigLatinException {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedBySpace() throws PigLatinException {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingCompositeWords() throws PigLatinException {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedBySpaceAndCompositeWords() throws PigLatinException {
		String inputPhrase = "hello well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingExclamationMark() throws PigLatinException {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingCommaAndPoint() throws PigLatinException {
		String inputPhrase = "hello, test-driven-development has ended.";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay, esttay-ivendray-evelopmentday ashay endeday.", translator.translate());
	}
	
	@Test(expected = PigLatinException.class)
	public void testTranslationPhraseContainingDisallowedCharacters() throws PigLatinException {
		String inputPhrase = "hello$";
		Translator translator = new Translator(inputPhrase);
		translator.translate();
	}
	
	
}
